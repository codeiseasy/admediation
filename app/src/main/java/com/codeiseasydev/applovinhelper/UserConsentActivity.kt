package com.codeiseasydev.applovinhelper


import android.os.Bundle

import androidx.appcompat.app.AppCompatActivity
import com.codeiseasydev.applovinhelper.Constants.privacyPolicy
import com.codeiseasydev.applovinhelper.Constants.publisherId

import com.google.android.ads.consent.ConsentStatus
import com.google.android.ads.consent.enums.WindowMode
import com.google.android.ads.consent.listener.enums.AdContentRating
import com.google.android.ads.consent.listener.interfaces.UserConsentEventListener
import com.google.android.ads.consent.ui.UserConsent
import com.google.android.ads.consent.ui.UserConsentDialogStyle

import com.google.android.gms.ads.AdRequest


open class UserConsentActivity : AppCompatActivity() {
    private var userConsent: UserConsent? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        var style = UserConsentDialogStyle(this)
        style.setTopBarDisplayIcon(false)
        style.setButtonPersonalizedIAgreeText("Yes, I Accept")
        style.setButtonPersonalizedDisagreeText("No, Thanks")
        style.setButtonPersonalizedAgreeText("Accept")
        style.setTopBarTextColor(resources.getColor(R.color.consent_top_bar_text_color))
        style.setPrivacyTextColor(resources.getColor(R.color.consent_privacy_text_color))
        style.setBodyTextColor(resources.getColor(R.color.consent_body_text_color))

        style.setTopBarBackgroundColor(resources.getColor(R.color.consent_top_bar_background_color))
        style.setButtonBackgroundColor(resources.getColor(R.color.consent_button_background_color))
        style.setButtonTextColor(resources.getColor(R.color.consent_button_text_color))

        style.setTopBarTextSize(18f)
        style.setBodyTextSize(16f)
        style.setPrivacyTextSize(16f)
        style.setTopBarTextFont("fonts/HacenTunisiaLt.ttf")
        style.setBodyTextFont("fonts/HacenTunisiaLt.ttf")
        style.setPrivacyTextFont("fonts/HacenTunisiaLt.ttf")
        style.setAccentColor(resources.getColor(R.color.consent_accent_color))
        style.setButtonCornerRadius(4)
        style.setAllCornerRadius(0)

        userConsent = UserConsent.Builder()
            .setContext(this)
            .setCaliforniaConsumerPrivacyAct(true)
            .setPrivacyUrl(privacyPolicy) // Add your privacy policy url
            .setPublisherId(publisherId) // Add your admob publisher id
            .setTestDeviceId(AdRequest.DEVICE_ID_EMULATOR) // Add your test device id "Remove addTestDeviceId on production!"
            .setLog(UserConsentActivity::class.java.name) // Add custom tag default: ID_LOG
            .setDebugGeography(true) // Geography appears as in EEA for test devices.
            .setTagForChildDirectedTreatment(false) // setTagForChildDirectedTreatment to true to indicate that you want your content treated as child-directed for purposes of COPPA.
            .setTagForUnderAgeOfConsent(true) //setTagForUnderAgeOfConsent to true to indicate that you want the ad request to be handled in a manner suitable for users under the age of consent.
            .setShowingWindowWithAnimation(true)
            .setShowingWindowMode(WindowMode.SHEET_BOTTOM)
            .setUserConsentStyle(style)
            .setMaxAdContentRating(AdContentRating.MA)
            .build()
    }

    fun checkUserConsent(callback: UserConsentEventListener?) {
        userConsent?.checkUserConsent(object : UserConsentEventListener {
            override fun onResult(consentStatus: ConsentStatus, isRequestLocationInEeaOrUnknown: Boolean) {
                callback?.onResult(consentStatus, isRequestLocationInEeaOrUnknown)
            }

            override fun onFailed(reason: String) {
                callback?.onFailed(reason)
            }
        })

    }

    fun revokeUserConsent() {
        userConsent?.revokeUserConsent()
    }

}
package com.codeiseasydev.applovinhelper

import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import com.applovin.adview.AppLovinAdView
import com.applovin.sdk.*
import com.codeiseasydev.applovinhelper.Constants.privacyPolicy
import com.codeiseasydev.applovinhelper.Constants.publisherId
import com.google.android.ads.consent.ui.UserConsent
import com.google.android.ads.consent.ui.UserConsentSettingsActivity
import com.google.android.ads.consent.util.LogDebug
import com.google.android.ads.mediation.audience.AppLovinHelper
import com.google.android.ads.mediation.listener.AdMobListener
import com.google.android.ads.mediation.listener.AdRewardedListener
import com.google.android.gms.ads.AdSize
import kotlinx.android.synthetic.main.activity_main.*


class AppLovinActivity : AppCompatActivity() {
    private var appLovinHelper: AppLovinHelper? = null
    private val buttonLoadInterstitialAd by lazy { findViewById<Button>(R.id.btn_load_interstitial) }
    private val buttonLoadRewardedVideoAd by lazy { findViewById<Button>(R.id.btn_load_rewarded_video) }
    private val buttonSettingConsentAd by lazy { findViewById<Button>(R.id.btn_setting_consent) }
    private val buttonRevokeConsentAd by lazy { findViewById<Button>(R.id.btn_revoke_consent) }

    private fun initViews(){
        buttonLoadInterstitialAd.setOnClickListener {
            appLovinHelper?.loadInterstitialAfterEndCount(it, 1, object : AdMobListener(){
                override fun onAdClosed() {
                    super.onAdClosed()
                    LogDebug.d("APPLOVIN", "onAdClosed()")
                }
            })
            return@setOnClickListener
        }
        buttonLoadRewardedVideoAd.setOnClickListener {
            appLovinHelper?.loadRewardedVideoAfterEndCount(it, 1, object : AdRewardedListener() {
                override fun onRewardedVideoAdClosed() {
                    super.onRewardedVideoAdClosed()
                    LogDebug.d("APPLOVIN", "onAdClosed()")
                }
            })
            return@setOnClickListener
        }
        buttonSettingConsentAd.setOnClickListener {
            UserConsentSettingsActivity.start(this, privacyPolicy, publisherId)
        }
        buttonRevokeConsentAd.setOnClickListener {
            UserConsent.getInstance(this).revokeUserConsentDialog()
        }
    }

    private fun initAds(){
        appLovinHelper = AppLovinHelper.Builder()
            .setContext(this)
            .setDebugTestAds(true)
            .setAppUnitId(resources.getString(R.string.test_admob_app_unit_id))
            .setAdViewUnitId(resources.getString(R.string.test_admob_banner_unit_id))
            .setInterstitialUnitId(resources.getString(R.string.test_admob_interstitial_unit_id))
            .setRewardedVideoUnitId(resources.getString(R.string.test_admob_rewarded_video_unit_id))
            .setNativeAdvancedUnitId(resources.getString(R.string.test_admob_native_advance_unit_id))
            .setLoadAnimationBeforeLoadAd(true)
            .setLoadAnimationBeforeLoadAdAccentColor(R.color.colorPrimaryDark)
            .setAdViewSize(AdSize.BANNER)
            .setEnableAds(true)
            .setEnableBannerAd(true)
            .setEnableInterstitialAd(false)
            .setEnableRewardedVideoAd(false)
            .build()

        appLovinHelper?.loadAdView(unitAdView, object : AdMobListener() {
            override fun onAdFailedToLoad(errorCode: Int) {
                super.onAdFailedToLoad(errorCode)
                LogDebug.d("APPLOVIN", "onAdFailedToLoad(${errorCode})")
            }
        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initAds()
        initViews()
    }

    private fun loadApplovinBannerAd(){
       val adView = AppLovinAdView(AppLovinAdSize.BANNER, "YOUR_ZONE_ID",this)
       adView.id = ViewCompat.generateViewId()
       unitAdView.addView(adView)

        val isTablet = AppLovinSdkUtils.isTablet(this)
        val heightPx = AppLovinSdkUtils.dpToPx(this, if (isTablet) 90 else 50)
        adView.layoutParams = RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, heightPx)

        // Load an ad!
        adView.loadNextAd()

        //
        // Optional: Set listeners
        //
        adView.setAdLoadListener(object : AppLovinAdLoadListener {
            override fun adReceived(ad: AppLovinAd) {
                LogDebug.d("APP_LOVIN_AD", "Banner loaded")
            }

            override fun failedToReceiveAd(errorCode: Int) {
                // Look at AppLovinErrorCodes.java for list of error codes
                LogDebug.d("APP_LOVIN_AD", "Banner failed to load with error code $errorCode")
            }
        })
    }

    override fun onPause() {
        super.onPause()
        appLovinHelper?.pause()
    }

    override fun onResume() {
        super.onResume()
        appLovinHelper?.resume()
    }

    override fun onDestroy() {
        appLovinHelper?.destroy()
        super.onDestroy()
    }
}
